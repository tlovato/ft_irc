S_NAME = serveur 
C_NAME = client 

S_SRCS_DIR = srcs_serveur/
C_SRCS_DIR = srcs_client/
COMMON_SRCS_DIR = srcs_common/

S_SRCS_FILES = main.c main_loop.c create_server.c read.c write.c commands.c msg.c rbuf.c
C_SRCS_FILES = main.c main_loop.c read_write.c commands.c
COMMON_SRCS_FILES = usage.c get_port.c get_max_fds.c init_fds_tab.c

S_SRCS = $(addprefix $(S_SRCS_DIR), $(S_SRCS_FILES))
C_SRCS = $(addprefix $(C_SRCS_DIR), $(C_SRCS_FILES))
COMMON_SRCS = $(addprefix $(COMMON_SRCS_DIR), $(COMMON_SRCS_FILES))

S_OBJS = $(S_SRCS:.c=.o)
C_OBJS = $(C_SRCS:.c=.o)
COMMON_OBJS = $(COMMON_SRCS:.c=.o)

all : $(S_NAME) $(C_NAME)

%.o: %.c
	gcc -g3 -c $< -o $@

$(S_NAME): $(COMMON_OBJS) $(S_OBJS)
	make -C libft/
	gcc -lncurses -g3 -Wall -Wextra -Werror -o $@ $^ -L libft/ -lft -fsanitize=address

$(C_NAME): $(COMMON_OBJS) $(C_OBJS)
	make -C libft/
	gcc -lncurses -g3 -Wall -Wextra -Werror -o $@ $^ -L libft/ -lft

clean:
	rm -f $(S_OBJS) $(C_OBJS) $(COMMON_OBJS)
	cd libft/ && make clean

fclean: clean
	rm -rf $(S_NAME) $(C_NAME)
	cd libft/ && make fclean

re: fclean all

.PHONY: all clean fclean re client serveur