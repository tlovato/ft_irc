/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 14:52:30 by tlovato           #+#    #+#             */
/*   Updated: 2019/05/30 14:52:31 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../srcs_common/ft_irc.h"
#include "client.h"

int						create_client(char *m, int port, t_env *env, t_fd *fds)
{
	int					serveur;
	struct protoent		*proto;
	struct sockaddr_in	sin;

	if (!(proto = getprotobyname("tcp")))
		return (-1);
	serveur = socket(PF_INET, SOCK_STREAM, proto->p_proto);
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	sin.sin_addr.s_addr = inet_addr(m);
	if ((connect(serveur, (const struct sockaddr *)&sin, sizeof(sin))) == -1)
	{
		close(serveur);
		return (usage("./client : try again later.\n"));
	}
	printf("Connected to server.\n/help : list of commands\n");
	fds[serveur].type = FD_SERVEUR;
	return (serveur);
}

int						main(int ac, char **av)
{
	int					port;
	int					client;
	t_env				env;
	t_fd				*fds;

	env.maxfds = get_max_fds();
	if (!(av[1]) || !(av[2]))
		return (usage("Usage : ./client <machine> <port>\n"));
	if (av[3])
		return (usage("Usage : ./client <machine> <port>\n"));
	if ((port = get_port(av[2])) == -1)
		return (-1);
	if ((fds = init_fds_tab(env.maxfds)) == NULL)
		return (-1);
	if ((env.serveur = create_client(av[1], port, &env, fds)) == -1)
		return (-1);
	main_loop(&env, fds);
	close(env.serveur);
	return (0);
}
