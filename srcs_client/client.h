/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 14:49:38 by tlovato           #+#    #+#             */
/*   Updated: 2019/05/30 14:49:40 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CLIENT_H
# define CLIENT_H

void		prompt(int client);
void		main_loop(t_env *env, t_fd *fds);
void		client_write(t_env *env, t_fd *fds);
void		client_read(t_env *env, t_fd *fds);
void		help(void);
void		who(t_env *env, t_fd *fds);
void		msg(t_env *env, t_fd *fds);
void		cmd_connect(t_env *env, t_fd *fds);
int			create_client(char *machine, int port, t_env *env, t_fd *fds);

#endif
