/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_loop.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 14:52:11 by tlovato           #+#    #+#             */
/*   Updated: 2019/05/30 14:52:12 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../srcs_common/ft_irc.h"
#include "client.h"

static void		init_fds(t_env *env)
{
	FD_ZERO(&env->fd_read);
	FD_ZERO(&env->fd_write);
	FD_SET(0, &env->fd_read);
	FD_SET(env->serveur, &env->fd_read);
}

static void		check_fds(t_env *env, t_fd *fds)
{
	if (FD_ISSET(0, &env->fd_read))
		client_write(env, fds);
	else if (FD_ISSET(env->serveur, &env->fd_read))
		client_read(env, fds);
}

void			main_loop(t_env *env, t_fd *fds)
{
	int			ret;

	while (1)
	{
		write(0, "$> ", 3);
		init_fds(env);
		if (select(env->serveur + 1, &env->fd_read, &env->fd_write, NULL, NULL))
			check_fds(env, fds);
	}
}
