/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   commands.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 14:50:18 by tlovato           #+#    #+#             */
/*   Updated: 2019/05/30 14:50:20 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../srcs_common/ft_irc.h"
#include "client.h"

void		help(void)
{
	printf("List of commands : \n");
	printf("	/nick [nickname] : change nickname.\n");
	printf("	/join [#chan] : join channel [chan].\n");
	printf("	/leave [#chan] : leave channel [chan].\n");
	printf("	/who : list users logged on channel.\n");
	printf("	/msg [nickname] [message] : send message to a user.\n");
	printf("	/connect [machine] [port] : connect to server.\n");
}

void		who(t_env *env, t_fd *fds)
{
	char	buf[BUF_SIZE];
	int		len;

	ft_bzero(buf, BUF_SIZE);
	while (recv(env->serveur, &len, sizeof(int), 0) > 0)
	{
		recv(env->serveur, buf, len, 0);
		if (buf[0] == '/')
			break ;
		printf("	%s\n", buf);
		ft_bzero(buf, BUF_SIZE);
	}
}

void		msg(t_env *env, t_fd *fds)
{
	char	buf[BUF_SIZE];

	ft_bzero(buf, BUF_SIZE);
	recv(env->serveur, buf, BUF_SIZE, 0);
	printf("[MP] : %s\n", buf);
}

void		cmd_connect(t_env *env, t_fd *fds)
{
	char	machine[BUF_SIZE];
	char	port[BUF_SIZE];
	int		len;

	ft_bzero(machine, BUF_SIZE);
	ft_bzero(port, BUF_SIZE);
	recv(env->serveur, &len, sizeof(int), 0);
	recv(env->serveur, machine, len, 0);
	recv(env->serveur, &len, sizeof(int), 0);
	recv(env->serveur, port, len, 0);
	close(env->serveur);
	fds[env->serveur].type = FD_FREE;
	fds[env->serveur].ft_read = NULL;
	fds[env->serveur].ft_write = NULL;
	create_client(machine, ft_atoi(port), env, fds);
}
