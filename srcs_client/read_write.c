/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_write.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 14:53:32 by tlovato           #+#    #+#             */
/*   Updated: 2019/05/30 14:53:34 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../srcs_common/ft_irc.h"
#include "client.h"

void			client_write(t_env *env, t_fd *fds)
{
	char		buf[BUF_SIZE];

	ft_bzero(buf, BUF_SIZE);
	if (read(0, &buf, BUF_SIZE))
		send(env->serveur, buf, ft_strlen(buf), 0);
	else
		exit(0);
}

static void		parse_commands(t_env *env, char *cmd, t_fd *fds)
{
	if (!ft_strcmp(cmd, "/h"))
		help();
	else if (!ft_strcmp(cmd, "/w"))
		who(env, fds);
	else if (!ft_strcmp(cmd, "/m"))
		msg(env, fds);
	else if (!ft_strcmp(cmd, "/c"))
		cmd_connect(env, fds);
}

void			client_read(t_env *env, t_fd *fds)
{
	char		nickname[BUF_SIZE];
	char		buf[BUF_SIZE];
	size_t		nick_len;

	ft_bzero(buf, BUF_SIZE);
	ft_bzero(nickname, BUF_SIZE);
	if (!(recv(env->serveur, buf, 2, 0)))
		exit(0);
	if (buf[0] == '/')
		return (parse_commands(env, buf, fds));
	else
	{
		recv(env->serveur, &nick_len, sizeof(size_t), 0);
		recv(env->serveur, nickname, (int)nick_len, 0);
		recv(env->serveur, buf, BUF_SIZE, 0);
		printf("[%s] : %s", nickname, buf);
	}
}
