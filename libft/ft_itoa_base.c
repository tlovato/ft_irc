/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 11:22:47 by tlovato           #+#    #+#             */
/*   Updated: 2019/04/12 11:22:48 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		len(long n, int base)
{
	int			ret;

	ret = (n < 0 && base == 10) ? 1 : 0;
	if (!n)
		return (1);
	while (n)
	{
		n /= base;
		ret++;
	}
	return (ret);
}

static char		*conversion(long n, int base, char *ret, int nlen)
{
	char		*tab;

	tab = "0123456789ABCDEF";
	ret[nlen] = '\0';
	if (n < 0 && base == 10)
	{
		n = -n;
		ret[0] = '-';
	}
	while (n > 0)
	{
		ret[--nlen] = tab[n % base];
		n /= base;
	}
	return (ret);
}

char			*ft_itoa_base(long n, int base)
{
	char		*ret;
	int			nlen;

	nlen = len(n, base);
	if (base < 2 || base > 16)
		return (NULL);
	if (!(ret = (char *)malloc(sizeof(char) * (nlen + 1))))
		return (NULL);
	if (!n)
		return (ret = "0");
	return (conversion(n, base, ret, nlen));
}
