/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_server.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 14:12:35 by tlovato           #+#    #+#             */
/*   Updated: 2019/05/30 14:12:36 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "serveur.h"
#include "../srcs_common/ft_irc.h"

static int				connect_to_client(t_env *env, int i, t_fd *fds)
{
	struct sockaddr_in	csin;
	unsigned int		cslen;
	int					client;
	char				*nickname;

	cslen = sizeof(csin);
	client = accept(env->serveur, (struct sockaddr *)&csin, &cslen);
	if (client == -1)
		return (usage("./serveur : try again later.\n"));
	printf("Client#%d connected - channel #general.\n", client - 3);
	fds[client].type = FD_CLIENT;
	fds[client].ft_read = client_read;
	fds[client].ft_read = client_write;
	nickname = ft_strjoinfree("Client#", ft_itoa(client - 3), 2);
	fds[client].user.nickname = nickname;
	fds[client].user.channel = ft_strdup("#general");
	return (client);
}

int						create_serveur(int port, t_env *env, t_fd *fds)
{
	int					serveur;
	struct protoent		*proto;
	struct sockaddr_in	sin;

	if (!(proto = getprotobyname("tcp")))
		return (-1);
	serveur = socket(PF_INET, SOCK_STREAM, proto->p_proto);
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	sin.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(serveur, (const struct sockaddr *)&sin, sizeof(sin)) < 0)
	{
		printf("Bind Error\n");
		exit(0);
	}
	listen(serveur, 42);
	fds[serveur].type = FD_SERVEUR;
	fds[serveur].ft_read = connect_to_client;
	return (serveur);
}
