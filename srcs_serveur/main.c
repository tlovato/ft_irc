/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 14:16:17 by tlovato           #+#    #+#             */
/*   Updated: 2019/05/30 14:16:18 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "serveur.h"
#include "../srcs_common/ft_irc.h"

int				main(int ac, char **av)
{
	int			port;
	t_env		env;
	t_fd		*fds;

	env.maxfds = get_max_fds();
	if (!(av[1]))
		return (usage("Usage : ./serveur <port>\n"));
	if (av[2])
		return (usage("Usage : ./serveur <port>\n"));
	if ((port = get_port(av[1])) == -1)
		return (-1);
	if ((fds = init_fds_tab(env.maxfds)) == NULL)
		return (-1);
	if ((env.serveur = create_serveur(port, &env, fds)) == -1)
		return (-1);
	if ((main_loop(&env, fds)) == -1)
		return (-1);
	close(env.serveur);
	close(env.client);
	return (0);
}
