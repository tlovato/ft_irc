/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_loop.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 14:15:06 by tlovato           #+#    #+#             */
/*   Updated: 2019/05/30 14:15:07 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../srcs_common/ft_irc.h"
#include "serveur.h"

static void			init_fds(t_env *env, t_fd *fds)
{
	int				i;

	i = 0;
	env->max = 0;
	FD_ZERO(&env->fd_read);
	FD_ZERO(&env->fd_write);
	while (i < env->maxfds)
	{
		if (fds[i].type != FD_FREE)
		{
			FD_SET(i, &env->fd_read);
			if (ft_strlen(fds[i].buf_write) > 0)
				FD_SET(i, &env->fd_write);
			env->max = MAX(env->max, i);
		}
		i++;
	}
}

static void			check_fds(t_env *env, t_fd *fds)
{
	int				i;

	i = 0;
	while (i < env->maxfds && env->nfds > 0)
	{
		if (FD_ISSET(i, &env->fd_read))
			fds[i].ft_read(env, i, fds);
		if (FD_ISSET(i, &env->fd_write))
			fds[i].ft_write(env, i, fds);
		if (FD_ISSET(i, &env->fd_read) || FD_ISSET(i, &env->fd_write))
			env->nfds--;
		i++;
	}
}

int					main_loop(t_env *e, t_fd *fds)
{
	while (1)
	{
		init_fds(e, fds);
		e->nfds = select(e->max + 1, &e->fd_read, &e->fd_write, NULL, NULL);
		check_fds(e, fds);
	}
}
