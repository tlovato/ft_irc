/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   commands.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 14:10:31 by tlovato           #+#    #+#             */
/*   Updated: 2019/05/30 17:43:57 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "serveur.h"
#include "../srcs_common/ft_irc.h"

void		nick(char **cmd, t_env *env, int i, t_fd *fds)
{
	int		j;

	j = 0;
	if (cmd[2] || !cmd[1])
		return ;
	while (j < ft_strlen(cmd[1]) - 1)
	{
		if (!ft_isalnum(cmd[1][j]))
			return ;
		j++;
	}
	cmd[1][ft_strlen(cmd[1]) - 1] = '\0';
	printf("%s changed nickname to : %s\n", fds[i].user.nickname, cmd[1]);
	free(fds[i].user.nickname);
	fds[i].user.nickname = ft_strdup(cmd[1]);
}

void		join(char **cmd, t_env *env, int i, t_fd *fds)
{
	if (cmd[2] || cmd[1][0] != '#')
		return ;
	cmd[1][ft_strlen(cmd[1]) - 1] = '\0';
	printf("%s joined channel : %s\n", fds[i].user.nickname, cmd[1]);
	free(fds[i].user.channel);
	fds[i].user.channel = ft_strdup(cmd[1]);
}

void		leave(char **cmd, t_env *env, int i, t_fd *fds)
{
	if (cmd[2] || cmd[1][0] != '#')
		return ;
	cmd[1][ft_strlen(cmd[1]) - 1] = '\0';
	if (ft_strcmp(cmd[1], fds[i].user.channel))
		return ;
	printf("%s left channel : %s", fds[i].user.nickname, cmd[1]);
	printf(" - back to channel #general\n");
	free(fds[i].user.channel);
	fds[i].user.channel = ft_strdup("#general");
}

void		who(t_env *env, int i, t_fd *fds)
{
	int		j;
	int		len;

	j = 0;
	send(i, "/w", 2, 0);
	while (j <= env->max)
	{
		if (fds[j].user.channel &&
			!ft_strcmp(fds[j].user.channel, fds[i].user.channel))
		{
			len = ft_strlen(fds[j].user.nickname);
			send(i, &len, sizeof(int), 0);
			send(i, fds[j].user.nickname, len, 0);
		}
		j++;
	}
	len = 5;
	send(i, &len, sizeof(int), 0);
	send(i, "/stop", 2, 0);
}
