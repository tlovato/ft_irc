/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rbuf.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/01 18:50:57 by tlovato           #+#    #+#             */
/*   Updated: 2019/06/01 18:50:58 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "serveur.h"
#include "../srcs_common/ft_irc.h"

static void			send_msg(int i, t_env *env, t_fd *fds, char *buf)
{
	size_t			nick_len;
	int				j;

	j = 0;
	while (j <= env->maxfds)
	{
		if (fds[j].type == FD_CLIENT && j != i
			&& !ft_strcmp(fds[j].user.channel, fds[i].user.channel))
		{
			send(j, "ms", 2, 0);
			nick_len = ft_strlen(fds[i].user.nickname);
			send(j, &nick_len, sizeof(size_t), 0);
			send(j, fds[i].user.nickname, nick_len, 0);
			send(j, buf, ft_strlen(buf), 0);
		}
		j++;
	}
}

static void			rbuf_write(int idx, t_env *env, t_fd *fds)
{
	char			buf[BUF_SIZE + 1];
	size_t			i;
	size_t			j;

	i = 0;
	j = 0;
	ft_bzero(buf, BUF_SIZE + 1);
	if (fds[idx].buf_read[ft_strlen(fds[idx].buf_read) - 1] == '\n')
		send_msg(idx, env, fds, fds[idx].buf_read);
	else
	{
		while (fds[idx].buf_read[i] != '\n')
			i++;
		i++;
		while (j < ft_strlen(fds[idx].buf_read))
		{
			if (i > BUF_SIZE)
				i = 0;
			buf[j++] = fds[idx].buf_read[i++];
		}
		send_msg(idx, env, fds, buf);
	}
}

void				rbuf_read(int idx, t_env *env, t_fd *fds)
{
	static char		buf[BUF_SIZE + 1];
	size_t			i;
	static int		j = -1;

	i = 0;
	if (j == -1)
	{
		ft_bzero(buf, BUF_SIZE + 1);
		j = 0;
	}
	while (fds[idx].buf_read[i])
	{
		if (j > BUF_SIZE)
			j = 0;
		buf[j++] = fds[idx].buf_read[i++];
	}
	if (ft_strchr(buf, '\n'))
	{
		rbuf_write(idx, env, fds);
		ft_bzero(buf, BUF_SIZE + 1);
		j = 0;
	}
}
