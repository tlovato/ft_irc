/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   serveur.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 14:17:21 by tlovato           #+#    #+#             */
/*   Updated: 2019/05/30 14:17:23 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SERVEUR_H
# define SERVEUR_H

# include "../srcs_common/ft_irc.h"

int		main_loop(t_env *env, t_fd *fds);
int		create_serveur(int port, t_env *env, t_fd *fds);
int		client_write(t_env *env, int i, t_fd *fds);
int		client_read(t_env *env, int i, t_fd *fds);
void	nick(char **cmd, t_env *env, int i, t_fd *fds);
void	join(char **cmd, t_env *env, int i, t_fd *fds);
void	leave(char **cmd, t_env *env, int i, t_fd *fds);
void	who(t_env *env, int i, t_fd *fds);
void	msg(char **cmd, t_env *env, int i, t_fd *fds);
void	rbuf_read(int idx, t_env *env, t_fd *fds);

#endif
