/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   write.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 14:17:47 by tlovato           #+#    #+#             */
/*   Updated: 2019/05/30 14:17:48 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "serveur.h"
#include "../srcs_common/ft_irc.h"

static void		cmd_connect(char **cmd, t_env *env, int i)
{
	int			len;

	if (!cmd[1] || !cmd[2] || cmd[3])
		return ;
	send(i, "/c", 2, 0);
	len = ft_strlen(cmd[1]);
	send(i, &len, sizeof(int), 0);
	send(i, cmd[1], len, 0);
	len = ft_strlen(cmd[2]);
	send(i, &len, sizeof(int), 0);
	send(i, cmd[2], len, 0);
}

static int		parse_commands(t_env *env, int i, t_fd *fds)
{
	char		**cmd;

	cmd = ft_strsplit(fds[i].buf_read, ' ');
	if (!ft_strncmp(cmd[0], "/nick", 5))
		nick(cmd, env, i, fds);
	else if (!ft_strncmp(cmd[0], "/help", 5))
		send(i, "/h", 2, 0);
	else if (!ft_strncmp(cmd[0], "/join", 5))
		join(cmd, env, i, fds);
	else if (!ft_strncmp(cmd[0], "/leave", 6))
		leave(cmd, env, i, fds);
	else if (!ft_strncmp(cmd[0], "/who", 4))
		who(env, i, fds);
	else if (!ft_strncmp(cmd[0], "/msg", 4))
		msg(cmd, env, i, fds);
	else if (!ft_strncmp(cmd[0], "/connect", 8))
		cmd_connect(cmd, env, i);
	else
	{
		cmd[0][ft_strlen(cmd[0]) - 1] = '\0';
		printf("%s : unknown command\n", cmd[0]);
	}
	ft_free_str_tab(cmd);
	return (0);
}

int				client_write(t_env *env, int i, t_fd *fds)
{
	int			r;

	ft_bzero(fds[i].buf_read, BUF_SIZE);
	if ((r = recv(i, fds[i].buf_read, BUF_SIZE, 0)) > 0)
	{
		if (fds[i].buf_read[0] == '/')
			return (parse_commands(env, i, fds));
		fds[i].buf_read[r] = '\0';
		rbuf_read(i, env, fds);
		ft_bzero(fds[i].buf_read, BUF_SIZE);
	}
	else
	{
		close(i);
		printf("%s disconnected.\n", fds[i].user.nickname);
		fds[i].type = FD_FREE;
		fds[i].ft_read = NULL;
		fds[i].ft_write = NULL;
	}
	return (0);
}
