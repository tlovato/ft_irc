/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   msg.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 17:47:14 by tlovato           #+#    #+#             */
/*   Updated: 2019/05/30 17:47:15 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "serveur.h"
#include "../srcs_common/ft_irc.h"

static char		*get_msg(char **cmd)
{
	int			i;
	char		*msg;

	i = 2;
	msg = ft_strnew(1);
	while (cmd[i])
	{
		msg = ft_strjoinfree(msg, " ", 1);
		msg = ft_strjoinfree(msg, cmd[i], 1);
		i++;
	}
	return (msg);
}

void			msg(char **cmd, t_env *env, int i, t_fd *fds)
{
	int			j;
	char		*msg;

	j = 0;
	if (!cmd[2])
		return ;
	msg = get_msg(cmd);
	j = 0;
	while (j <= env->max)
	{
		if (fds[j].user.nickname
			&& !ft_strcmp(cmd[1], fds[j].user.nickname))
		{
			send(j, "/m", 2, 0);
			send(j, msg, ft_strlen(msg), 0);
		}
		j++;
	}
}
