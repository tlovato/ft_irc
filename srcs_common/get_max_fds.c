/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_max_fds.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 14:46:22 by tlovato           #+#    #+#             */
/*   Updated: 2019/05/30 14:46:23 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_irc.h"

int					get_max_fds(void)
{
	struct rlimit	rlp;

	getrlimit(RLIMIT_NOFILE, &rlp);
	return (rlp.rlim_cur);
}
