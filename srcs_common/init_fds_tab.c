/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_fds_tab.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 14:47:53 by tlovato           #+#    #+#             */
/*   Updated: 2019/05/30 14:47:54 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_irc.h"

t_fd			*init_fds_tab(int maxfds)
{
	int			i;
	t_fd		*ret;

	i = 0;
	if (!(ret = (t_fd *)malloc(sizeof(t_fd) * maxfds)))
		return (NULL);
	while (i < maxfds)
	{
		ret[i].type = FD_FREE;
		ret[i].ft_read = NULL;
		ret[i].ft_write = NULL;
		i++;
	}
	return (ret);
}
