/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_irc.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 14:20:34 by tlovato           #+#    #+#             */
/*   Updated: 2019/05/30 14:20:36 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_IRC_H
# define FT_IRC_H

# include "../libft/libft.h"
# include <unistd.h>
# include <netdb.h>
# include <sys/socket.h>
# include <netinet/in.h>
# include <arpa/inet.h>
# include <sys/select.h>
# include <sys/time.h>
# include <sys/types.h>
# include <stdio.h>
# include <sys/errno.h>

# define BUF_SIZE 4096

# define FD_FREE 0
# define FD_CLIENT 1
# define FD_SERVEUR 2

# define MAX(a,b) ((a > b) ? a : b)

typedef struct		s_env
{
	int				maxfds;
	int				serveur;
	int				client;
	fd_set			fd_read;
	fd_set			fd_write;
	int				nfds;
	int				max;
}					t_env;

typedef struct		s_user
{
	char			*nickname;
	char			*channel;
}					t_user;

typedef struct		s_fd
{
	int				type;
	int				(*ft_read)(t_env *, int, struct s_fd *);
	int				(*ft_write)(t_env *, int, struct s_fd *);
	char			buf_read[BUF_SIZE + 1];
	char			buf_write[BUF_SIZE + 1];
	t_user			user;
}					t_fd;

int					usage(char *str);
int					get_port(char *port);
t_fd				*init_fds_tab(int maxfds);
int					get_max_fds(void);

#endif
