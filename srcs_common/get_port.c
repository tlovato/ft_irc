/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_port.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 14:47:10 by tlovato           #+#    #+#             */
/*   Updated: 2019/05/30 14:47:11 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_irc.h"

int				get_port(char *port)
{
	int			i;
	int			ret;

	i = 0;
	while (port[i])
	{
		if (!ft_isdigit(port[i]))
			return (usage("./serveur : port invalide.\n"));
		i++;
	}
	ret = ft_atoi(port);
	return (ret);
}
